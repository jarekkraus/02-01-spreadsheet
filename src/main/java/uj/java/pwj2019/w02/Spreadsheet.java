package uj.java.pwj2019.w02;

public class Spreadsheet {

    private int ADD(int a, int b){
        return a + b;
    }

    private int SUB(int a, int b){
        return a - b;
    }

    private int MUL(int a, int b){
        return a * b;
    }

    private int DIV(int a, int b){
        return a / b;
    }

    private int MOD(int a, int b){
        return a % b;
    }

    private String eval(String[][] input, int row, int col) {
        String cell = input[row][col];
        if(cell.matches("\\d+")) {
            return cell;
        }else if(cell.matches("\\$[A-Z]\\d+")) {
            input[row][col] = eval(input,Integer.parseInt(cell.split("[A-Z]")[1])-1, cell.charAt(1)-65);
        }else if(cell.charAt(0) == '='){
            String operation = cell.substring(1, 4);

            String value1 = cell.split("\\(")[1].split("\\)")[0].split(",")[0];
            String value2 = cell.split("\\(")[1].split("\\)")[0].split(",")[1];

            if(value1.matches("\\$[A-Z]\\d+")) {
                value1 = eval(input,Integer.parseInt(value1.split("[A-Z]")[1])-1, value1.charAt(1)-65);
            }

            if(value2.matches("\\$[A-Z]\\d+")) {
                value2 = eval(input,Integer.parseInt(value2.split("[A-Z]")[1])-1, value2.charAt(1)-65);
            }

            switch (operation) {
                case "ADD":
                    input[row][col] = Integer.toString(ADD(Integer.parseInt(value1), Integer.parseInt(value2)));
                    break;
                case "SUB":
                    input[row][col] = Integer.toString(SUB(Integer.parseInt(value1), Integer.parseInt(value2)));
                    break;
                case "MUL":
                    input[row][col] = Integer.toString(MUL(Integer.parseInt(value1), Integer.parseInt(value2)));
                    break;
                case "DIV":
                    input[row][col] = Integer.toString(DIV(Integer.parseInt(value1), Integer.parseInt(value2)));
                    break;
                case "MOD":
                    input[row][col] = Integer.toString(MOD(Integer.parseInt(value1), Integer.parseInt(value2)));
                    break;
            }
        }
        return input[row][col];
    }

    public String[][] calculate(String[][] input) {
        for(int i=0; i<input.length; i++)
            for(int j=0; j<input[i].length; j++)
                eval(input,i,j);
        return input;
    }

}
